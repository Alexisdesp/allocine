// Liste des seances par cinéma avec les films associés
import { Film } from './film';
import { Seance } from './seance';


export const seances: Seance[] = [
    {
        id: 1,
        jour: 'Lundi / mardi / jeudi / Vendredi',
        horaire: { hours: 10, minutes: 30 },
        salle: '1',
        films: [
            {
                id: 1,
                titre: 'D.A.R.Y.L.',
                duree: { hours: 1, minutes: 39 },
                image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
                cinemas: []
            }
        ]
    },
    {
        id: 2,
        jour: 'Lundi / mardi / jeudi / Vendredi',
        horaire: { hours: 12, minutes: 30 },
        salle: '2',
        films: [
            {
                id: 2,
        titre: 'USUAL SUSPECTS',
        duree: {hours: 1, minutes: 46},
        image: '69199504_af.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
                cinemas: []
            }
        ]
    },
    {
        id: 3,
        jour: 'Lundi / mardi / jeudi / Vendredi',
        horaire: { hours: 12, minutes: 30 },
        salle: '2',
        films: [
            {
                id: 3,
                titre: 'LA REINE DES NEIGES II',
                duree: {hours: 1, minutes: 44},
                image: '5952325.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
                cinemas: []
            }
        ]
    }
]

