import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeanceInfoComponent } from './seance-info.component';

describe('SeanceInfoComponent', () => {
  let component: SeanceInfoComponent;
  let fixture: ComponentFixture<SeanceInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeanceInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeanceInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
