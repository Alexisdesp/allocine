import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-seance-info',
  templateUrl: './seance-info.component.html',
  styleUrls: ['./seance-info.component.css']
})
export class SeanceInfoComponent implements OnInit {
  @Input() seance;
  constructor() { }

  ngOnInit() {
  }

}
