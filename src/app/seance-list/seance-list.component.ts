import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FilmService } from '../film.service';
import { SeanceService } from '../seance.service';

import { Film } from '../film';
import { Seance } from '../seance';

@Component({
  selector: 'app-seance-list',
  templateUrl: './seance-list.component.html',
  styleUrls: ['./seance-list.component.css']
})
export class SeanceListComponent implements OnInit {
  film : Film;
  seances : Seance[];

  constructor(
    private route: ActivatedRoute,
    private filmService: FilmService,
    private seanceService: SeanceService,
  ) { }

  ngOnInit() {
    let id : number = +this.route.snapshot.paramMap.get('idFilm');
    
    this.seances = this.seanceService.getAll(id);
  }

}
