import { Injectable } from '@angular/core';

import { seances } from './mock-seances-films';
import { Film } from './film';
import { Seance } from './seance';

@Injectable({
  providedIn: 'root'
})
export class SeanceService {

  constructor() { }

  getAll(idFilm) : Seance[] {
    return seances.filter(function(seance) {
      return seance.films.find(film => film.id === idFilm) !== undefined
    });
  }

  get(idFilm, idSeance) : Seance {
    return null;
    //return films.find(element => element.id === id);
  }

}
