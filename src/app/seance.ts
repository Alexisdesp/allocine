import { Time } from '@angular/common';
import { Film } from './film';

export interface Seance {
    id: number;
    jour: string;
    horaire: Time;
    salle: string;
    films: Film[];
}